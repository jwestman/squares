extends Node

class Gamemode:
	extends Object

	enum Difficulty { SLOW, MEDIUM, FAST }

	var difficulty : int
	var columns : int

	func _init(difficulty: int,
				columns: int) -> void:
		self.difficulty = difficulty
		self.columns = columns

	static func from_string(gamemode_string: String) -> Gamemode:
		var data := gamemode_string.split(".")
		return Gamemode.new(int(data[1]), int(data[2]))

	# Returns all combinations of gamemodes that can be
	# accessed through the main menu.
	static func all_combinations() -> Array:
		return [
			Gamemode.new(Difficulty.SLOW, 4),
			Gamemode.new(Difficulty.SLOW, 5),
			Gamemode.new(Difficulty.SLOW, 6),

			Gamemode.new(Difficulty.MEDIUM, 4),
			Gamemode.new(Difficulty.MEDIUM, 5),
			Gamemode.new(Difficulty.MEDIUM, 6),

			Gamemode.new(Difficulty.FAST, 4),
			Gamemode.new(Difficulty.FAST, 5),
			Gamemode.new(Difficulty.FAST, 6),
		]

	static func get_default() -> Gamemode:
		return Gamemode.new(Difficulty.MEDIUM, 4)

	func to_string() -> String:
		return "gamemode.%s.%s" % [difficulty, columns]

	# Gets a human-readable description of the gamemode
	func to_description() -> String:
		var difficulty_str := ""
		match difficulty:
			Difficulty.SLOW: difficulty_str = "Slow"
			Difficulty.MEDIUM: difficulty_str = "Medium"
			Difficulty.FAST: difficulty_str = "Fast"

		return "%s, %s columns" % [difficulty_str, columns]


const PATH = "user://data.cfg"

var cfg

func set_current_gamemode(gamemode: Gamemode) -> void:
	print("setting gamemode: " + gamemode.to_string())
	cfg.set_value("general", "current_gamemode", gamemode.to_string())

func get_current_gamemode() -> Gamemode:
	var gamemode_string = cfg.get_value("general", "current_gamemode", null)
	if gamemode_string:
		return Gamemode.from_string(gamemode_string)
	else:
		return Gamemode.get_default()

func game_finished(gamemode: Gamemode, score: int) -> void:
	_add_val("general", "games_played")
	_add_val(gamemode.to_string(), "games_played")

	_add_val("general", "total_score", score)
	_add_val(gamemode.to_string(), "total_score", score)

	if is_high_score(gamemode, score):
		_set_high_score(gamemode, score)

func get_high_score(gamemode: Gamemode) -> int:
	return int(cfg.get_value(gamemode.to_string(), "highscore", 0))

func is_high_score(gamemode: Gamemode, score: int) -> bool:
	var high_score = get_high_score(gamemode)
	return score > high_score

func get_games_played(gamemode = null) -> int:
	if gamemode:
		return int(cfg.get_value(gamemode.to_string(), "games_played", 0))
	else:
		return int(cfg.get_value("general", "games_played", 0))

func get_total_score(gamemode = null) -> int:
	if gamemode:
		return int(cfg.get_value(gamemode.to_string(), "total_score", 0))
	else:
		return int(cfg.get_value("general", "total_score", 0))

func _init() -> void:
	cfg = ConfigFile.new()
	cfg.load(PATH)

func save() -> void:
	cfg.save(PATH)

func _add_val(section: String, key: String, diff := 1) -> int:
	var current = int(cfg.get_value(section, key, 0))
	current += diff
	cfg.set_value(section, key, current)
	return current

func _set_high_score(gamemode: Gamemode, score: int) -> void:
	cfg.set_value(gamemode.to_string(), "highscore", score)
	save()

