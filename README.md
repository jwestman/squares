# Squares
A simple game to waste your time on.

## How to Play
The idea is simple: Tap the squares in order and see how far you get!

## Bugs and Feature Requests
If you find a bug, please file an issue in this repository. Be specific about what is going wrong.

If you have a feature request, also file an issue. Be specific about what you'd like to see in the game. Note that I don't have a whole lot of time to work on new features; the game is intended to be simple, and I'm not going to add complex new mechanics.

Remember, Squares is open source. If you want to fork it and add your own crazy new ideas, go for it!

## License

The code for Squares is licensed under the MIT License. See `LICENSE.md`.

Font is Londrina Solid, by Marcelo Magalhães, downloaded from Google Fonts. Licensed under the Open Font License version 1.1.

